// Created by iWeb 3.0 local-build-20180420

setTransparentGifURL('Media/transparent.gif');
function applyEffects()
{
    var registry = IWCreateEffectRegistry();
    registry.registerEffects({
        shadow_0: new IWShadow({
            blurRadius: 5,
            offset: new IWPoint(5.2485, 6.0377),
            color: '#cad6f1',
            opacity: 0.750000
        }),
        stroke_0: new IWEmptyStroke()
    });
    registry.applyEffects();
}
function hostedOnDM()
{
    return false;
}
function onPageLoad()
{
    loadMozillaCSS('Welcome_files/WelcomeMoz.css')
    adjustLineHeightIfTooBig('id1');
    adjustFontSizeIfTooBig('id1');
    adjustLineHeightIfTooBig('id2');
    adjustFontSizeIfTooBig('id2');
    adjustLineHeightIfTooBig('id3');
    adjustFontSizeIfTooBig('id3');
    detectBrowser();
    Widget.onload();
    fixupAllIEPNGBGs();
    fixAllIEPNGs('Media/transparent.gif');
    applyEffects()
}
function onPageUnload()
{
    Widget.onunload();
}
